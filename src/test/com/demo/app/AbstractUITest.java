package com.demo.app;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class AbstractUITest {
	protected WebDriver driver;
	protected static final String BASE_URL = "http://localhost:8080";

	public AbstractUITest() {
//		System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");
		// My path
		System.setProperty("webdriver.chrome.driver", "/home/sashqua/workspace/drivers/chromedriver");
	}

	@Before
	public void initDriver() throws InterruptedException {
		driver = new ChromeDriver();
	}

	@After
	public void tearDown() throws InterruptedException {
		Thread.sleep(1000); // Let the user actually see something!
		driver.quit();
	}
}
