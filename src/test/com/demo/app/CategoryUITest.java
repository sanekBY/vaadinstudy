package com.demo.app;

import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class CategoryUITest extends AbstractUITest {

	private static final int SLEEP_TIME = 1000;

	@Test
	public void testCategoryPage() throws InterruptedException {
		driver.get(BASE_URL);
		Thread.sleep(SLEEP_TIME); // Let the user actually see something!

		// go to category page
		WebElement categoryMenu = driver.findElement(By.xpath(
				"//span[@class='v-menubar-menuitem'][./span/@class='v-menubar-menuitem-caption'][contains(./span/text(), 'Hotel categories')]"));

		categoryMenu.click();
		Thread.sleep(SLEEP_TIME); // Let the user actually see something!

		Assert.assertEquals("http://localhost:8080/#!categories", driver.getCurrentUrl());
	}

	@Test
	public void testFilter() throws InterruptedException {
		driver.get(BASE_URL);
		Thread.sleep(SLEEP_TIME); // Let the user actually see something!

		// input value
		driver.findElement(By.xpath("//input[@placeholder='filter by name...']")).sendKeys("Dream");
		Thread.sleep(SLEEP_TIME);
	}

	@Test
	public void testCreationCategory() throws InterruptedException {
		driver.get(BASE_URL);
		Thread.sleep(SLEEP_TIME); // Let the user actually see something!

		// go to category page
		WebElement categoryMenu = driver.findElement(By.xpath("//span[@class='v-menubar-menuitem'][./span/@class='v-menubar-menuitem-caption'][contains(./span/text(), 'Hotel categories')]"));

		categoryMenu.click();
		Thread.sleep(SLEEP_TIME); // Let the user actually see something!

		addHotel("One");
		addHotel("Two");
		addHotel("Three");

	}

	/**
	 * Add hotels
	 * @param hotelName
	 * @throws InterruptedException
	 */
	public void addHotel(String hotelName) throws InterruptedException {
		WebElement addButton = driver.findElement(By.xpath("//div[@class='v-button v-widget'][@tabindex='0']"));
		addButton.click();
		Thread.sleep(SLEEP_TIME);

		WebElement inputCategory = driver.findElement(By.xpath("//input[@type='text'][@tabindex='0']"));
		inputCategory.sendKeys(hotelName);
		Thread.sleep(SLEEP_TIME);

		WebElement saveButton = driver.findElement(By.xpath("//div[@class='v-button v-widget primary v-button-primary'][@tabindex='0']"));
		saveButton.click();
		Thread.sleep(SLEEP_TIME);
	}


	@Test
	public void testCreationHotels() throws InterruptedException {
		driver.get(BASE_URL);
		Thread.sleep(SLEEP_TIME); // Let the user actually see something!

		generateHotels();

	}

	private void generateHotels() throws InterruptedException {

		for (int i = 1; i < 4; ++i) {
			// Add new hotel
			driver.findElement(By.xpath("//div[@class='v-slot'][./div/@class='v-button v-widget'][./div/@tabindex='0']")).click();
			Thread.sleep(SLEEP_TIME);

			//Inputs in form
			List<WebElement> elems = driver.findElements(By.xpath("//td[@class='v-formlayout-contentcell']//input"));

			elems.get(0).sendKeys("Name " + i);
			Thread.sleep(SLEEP_TIME);

			elems.get(1).sendKeys("Addres " + i);
			Thread.sleep(SLEEP_TIME);

			elems.get(2).clear();
			elems.get(2).sendKeys(String.valueOf(i));
			Thread.sleep(SLEEP_TIME);

			new Select(driver.findElement(By.xpath("//select[@class='v-select-select']"))).selectByVisibleText("One");

			elems.get(5).clear();
			elems.get(5).sendKeys("11/" + i +"/11");
			Thread.sleep(SLEEP_TIME);


			elems.get(6).sendKeys("Test URL " + i);
			Thread.sleep(SLEEP_TIME);


			elems.get(7).sendKeys("Test Description " + i);
			Thread.sleep(SLEEP_TIME);

			// Save button
			driver.findElement(By.xpath("//*[@id='ROOT-2521314']/div/div[2]/div[3]/div/div/div[3]/div/div/div[3]/div/table/tbody/tr[9]/td[3]/div/div/div")).click();
		}

	}




}
