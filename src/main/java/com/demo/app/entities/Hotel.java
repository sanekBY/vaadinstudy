package com.demo.app.entities;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Table(name = "HOTEL")
@NamedQueries({
		@NamedQuery(name="Hotel.findAll",
				query="SELECT h FROM Hotel h"),
		@NamedQuery(name="Hotel.findById",
				query="SELECT h FROM Hotel h WHERE h.id = :id"),
		@NamedQuery(name="Hotel.filterByNameAddress",
				query="SELECT h FROM Hotel h WHERE LOWER(h.name) LIKE :name and LOWER(h.address) LIKE :address"),
})
public class Hotel extends AbstractEntity implements Serializable, Cloneable {

	@Column(name = "NAME")
	private String name = "";

	@Column(name = "ADDRESS")
	private String address = "";

	@Column(name = "RATING")
	private Integer rating;

	@Column(name = "OPERATES_FROM")
	private Long operatesFrom;

	@ManyToOne
    @JoinColumn(name = "CATEGORY_ID")
	private HotelCategory category;

	@Column(name = "URL")
	private String url;

	@Column(name = "DESCRIPTION")
	private String description;

	@Embedded
	private GuarantyFee guarantyFee;

	public boolean isPersisted() {
		return getId() != null;
	}

	@Override
	public String toString() {
		return name + " " + rating +"stars " + address;
	}

	@Override
	public Hotel clone() throws CloneNotSupportedException {
		return (Hotel) super.clone();
	}

	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	public Hotel() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}

	public Long getOperatesFrom() {
		return operatesFrom;
	}

	public void setOperatesFrom(Long operatesFrom) {
		this.operatesFrom = operatesFrom;
	}

	public HotelCategory getCategory() {
		return category;
	}

	public void setCategory(HotelCategory category) {
		this.category = category;
	}	

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Hotel(Long id, String name, String address, Integer rating, Long operatesFrom, HotelCategory category, String url, String description) {
		super();
		setId(id);
		this.name = name;
		this.address = address;
		this.rating = rating;
		this.operatesFrom = operatesFrom;
		this.category = category;
		this.url = url;
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public GuarantyFee getGuarantyFee() {
		return guarantyFee;
	}

	public void setGuarantyFee(GuarantyFee guarantyFee) {
		this.guarantyFee = guarantyFee;
	}
}