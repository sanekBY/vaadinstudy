package com.demo.app.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "CATEGORY")
@NamedQueries({
        @NamedQuery(name="HotelCategory.findAll",
                query="SELECT h FROM HotelCategory h"),
        @NamedQuery(name="HotelCategory.findById",
                query="SELECT h FROM HotelCategory h WHERE h.id = :id")
})
public class HotelCategory extends AbstractEntity implements Serializable, Cloneable {

    @Column(name = "NAME")
    private String name;
//
//    @OneToMany(mappedBy = "category", cascade = CascadeType.ALL)
//    private List<Hotel> hotelList;

    public HotelCategory() {
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public String toString() {
        return name;
    }

    public HotelCategory(Long id, String name) {
        setId(id);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

//
//    public List<Hotel> getHotelList() {
//        return hotelList;
//    }
//
//    public void setHotelList(List<Hotel> hotelList) {
//        this.hotelList = hotelList;
//    }
}
