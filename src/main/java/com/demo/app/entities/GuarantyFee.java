package com.demo.app.entities;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@SuppressWarnings("serial")
@Embeddable
public class GuarantyFee implements Serializable, Cloneable {

	@Column(name = "GUARANTY_FEE")
	private Integer guarantyFee = 0;

    public Integer getGuarantyFee() {
        return guarantyFee;
    }

    public void setGuarantyFee(Integer guarantyFee) {
        this.guarantyFee = guarantyFee;
    }
    public GuarantyFee () {
    }

    public GuarantyFee (GuarantyFee value) {
        super();
        if (value != null) {
            this.guarantyFee = value.getGuarantyFee();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        GuarantyFee that = (GuarantyFee) o;
        if (guarantyFee.equals(that.getGuarantyFee())) return true;
        else return false;

//        return guarantyFee != null ? guarantyFee.equals(that.guarantyFee) : that.guarantyFee == null;
    }
//
//    @Override
//    public int hashCode() {
//        return guarantyFee != null ? guarantyFee.hashCode() : 0;
//    }
}
