package com.demo.app.views;


import com.demo.app.entities.AbstractEntity;
import com.demo.app.entities.Hotel;
import com.demo.app.forms.HotelBulkUpdateForm;
import com.demo.app.forms.HotelForm;
import com.demo.app.services.EntityService;
import com.demo.app.services.HotelService;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import com.vaadin.ui.renderers.HtmlRenderer;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;

import javax.swing.text.Style;
import java.util.ArrayList;
import java.util.List;

/**
 * Hotel list view
 */
@SuppressWarnings("serial")
@SpringUI
public class HotelListView extends VerticalLayout implements View {

    private Grid<Hotel> grid = new Grid<>(Hotel.class);
    private TextField filterName = new TextField();
    private TextField filterAddress = new TextField();
    private HotelForm hotelForm = new HotelForm(this);
    private PopupView popup;

    private Button update;
    private Button cancel;

    private HotelBulkUpdateForm hotelBulkUpdateForm = new HotelBulkUpdateForm(this);

    private List<Hotel> selectedHotels = new ArrayList<>();

    /**
     * Buttons
     */
    private Button addHotel;
    private Button editHotel;
    private Button deleteHotel;
    private Button bulkUpdateHotel;

    public HotelListView() {
        setSizeFull();
        addComponent(createHotelListFilter());
        addComponent(createHotelListMain());
        hotelForm.setVisible(false);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {

    }

    /**
     * List of hotel layout
     */
    private Layout createHotelListMain() {

        grid.setSelectionMode(Grid.SelectionMode.MULTI);
        grid.setColumns("name", "description", "rating", "address", "category" );

        updateList();
        //Adding link
        Grid.Column<Hotel, String> htmlColumn =  grid.addColumn(hotel ->
                        "<a href='" + hotel.getUrl() + "' target='_blank'>link</a>",
                new HtmlRenderer());
        htmlColumn.setCaption("Link");

        HorizontalLayout main = new HorizontalLayout(grid, hotelForm);
        main.setSizeFull();
        grid.setSizeFull();
        main.setExpandRatio(grid, 1);

        // Selection of hotels
        grid.addSelectionListener(event -> {
            selectedHotels.clear();
            selectedHotels.addAll(event.getAllSelectedItems());
            checkSelectedHotel();
        });

        return main;
    }

    /**
     * Filter by name + address layout
     */
    private Layout createHotelListFilter() {
        filterName.setPlaceholder("filter by name...");
        filterName.addValueChangeListener(e -> updateList());
        filterName.setValueChangeMode(ValueChangeMode.LAZY);

        filterAddress.setPlaceholder("filter by address...");
        filterAddress.addValueChangeListener(e -> updateList());
        filterAddress.setValueChangeMode(ValueChangeMode.LAZY);

        Button clearfilterTextbtn = new Button(VaadinIcons.CLOSE_CIRCLE);
        clearfilterTextbtn.setDescription("Clearing filter");
        clearfilterTextbtn.addClickListener(e -> filterName.clear());
        clearfilterTextbtn.addClickListener(e -> filterAddress.clear());

        CssLayout filtering = new CssLayout();
        filtering.addComponents(filterName, filterAddress, clearfilterTextbtn);
        filtering.setStyleName(ValoTheme.LAYOUT_COMPONENT_GROUP);

        return new HorizontalLayout(filtering, createButtons());
    }

    private Layout createButtons() {
        Layout layout = new HorizontalLayout();

        addHotel = new Button("Add new");
        addHotel.addClickListener( e -> {
            hotelForm.setNewHotel();
        });

        deleteHotel = new Button("Delete");
        deleteHotel.addClickListener(e -> {
            hotelForm.deleteSelected(selectedHotels);
        });

        editHotel = new Button("Edit");
        editHotel.addClickListener(e -> {
            hotelForm.setHotel(selectedHotels.get(0));
        });

        hotelBulkUpdateForm.addComponent(createPopupButtons());
        popup = new PopupView(null, hotelBulkUpdateForm);
        popup.setHideOnMouseOut(false);
        bulkUpdateHotel = new Button("Bulk update");
        bulkUpdateHotel.addClickListener( e -> {
            popup.setPopupVisible(true);
        });

        checkSelectedHotel();

        layout.addComponents(addHotel);
        layout.addComponents(deleteHotel);
        layout.addComponents(editHotel);
        layout.addComponents(bulkUpdateHotel);
        layout.addComponents(popup);

        return layout;
    }

    public Layout createPopupButtons() {
        Layout layout = new HorizontalLayout();

        update = new Button("Update");
        update.addClickListener( e -> {
            hotelBulkUpdateForm.update(selectedHotels);
            popup.setPopupVisible(false);
            updateList();
        });

        cancel =  new Button("Cancel");
        cancel.addClickListener( e -> {
            popup.setPopupVisible(false);
        });

        layout.addComponents(update);
        layout.addComponents(cancel);

        return layout;
    }

    /**
     * Enable/Disable buttons
     */
    private void checkSelectedHotel() {
        hotelForm.setVisible(false);
        editHotel.setEnabled(selectedHotels.size() == 1);
        deleteHotel.setEnabled(selectedHotels.size() > 0);
        bulkUpdateHotel.setEnabled(selectedHotels.size() > 1);
    }

    public void updateList() {
//        List<Hotel> hotels = hotelService.findAll(filterName.getValue(), filterAddress.getValue());
        List<Hotel> hotels = EntityService.getHotelService().findAll(filterName.getValue(), filterAddress.getValue());
        grid.setItems(hotels);
    }

}