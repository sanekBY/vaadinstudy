package com.demo.app.views;

import com.demo.app.entities.HotelCategory;
import com.demo.app.forms.HotelCategoryForm;
import com.demo.app.services.EntityService;
import com.demo.app.services.HotelCategoryService;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;

import java.util.ArrayList;
import java.util.List;
@SuppressWarnings("serial")
@SpringUI
public class HotelCategoryView extends VerticalLayout implements View {

    private Grid<HotelCategory> grid = new Grid<>(HotelCategory.class);
    private HotelCategoryForm  categoryForm = new HotelCategoryForm(this);
    private List<HotelCategory> selectedCategories = new ArrayList<>();

    private Button addCategory;
    private Button editCategory;
    private Button deleteCategory;

    public HotelCategoryView() {
        setSizeFull();
        addComponents(createCategoryListMain(), createButtons());
        updateList();
        categoryForm.setVisible(false);
    }

    /**
     * List of categories layout
     */
    private Layout createCategoryListMain() {
        grid.setColumns("name");
        grid.setSelectionMode(Grid.SelectionMode.MULTI);

        HorizontalLayout main = new HorizontalLayout(grid, categoryForm);
        main.setSizeFull();
        grid.setSizeFull();
        main.setExpandRatio(grid, 1);

        grid.addSelectionListener(event -> {
            selectedCategories.clear();
            selectedCategories.addAll(event.getAllSelectedItems());
            checkSelectedCategory();
        });

        return main;
    }

    /**
     * Buttons layout
     */
    private Layout createButtons() {
        Layout layout = new HorizontalLayout();

        addCategory = new Button("Add new");
        addCategory.addClickListener( e -> {
            categoryForm.setCategory(new HotelCategory());
        });

        deleteCategory = new Button("Delete");
        deleteCategory.addClickListener(e -> {
            categoryForm.deleteSelected(selectedCategories);
        });

        editCategory = new Button("Edit");
        editCategory.addClickListener(e -> {
            categoryForm.setCategory(selectedCategories.get(0));
        });

        checkSelectedCategory();

        layout.addComponents(addCategory, editCategory, deleteCategory);

        return layout;
    }

    /**
     * Enable/Disable buttons
     */
    private void checkSelectedCategory() {
        editCategory.setEnabled(selectedCategories.size() == 1);
        deleteCategory.setEnabled(selectedCategories.size() > 0);
    }

    /**
     * update list of categories
     */
    public void updateList() {
        List<HotelCategory> categoryList = EntityService.getHotelCategoryService().findAll();
        grid.setItems(categoryList);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {}
}
