package com.demo.app.forms;

import com.demo.app.entities.HotelCategory;
import com.demo.app.services.EntityService;
import com.demo.app.services.HotelCategoryService;
import com.demo.app.views.HotelCategoryView;
import com.vaadin.data.Binder;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.event.ShortcutAction;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.ValoTheme;

import java.util.List;

@SuppressWarnings("serial")
@SpringUI
public class HotelCategoryForm extends FormLayout {
    private TextField name = new TextField("Name");
    private Button save = new Button("Save");

    private HotelCategory category;
    private HotelCategoryView hotelCategoryView;
    private Binder<HotelCategory> binder = new Binder<>(HotelCategory.class);

    public HotelCategoryForm(HotelCategoryView hotelCategoryView) {
        this.hotelCategoryView = hotelCategoryView;

        setSizeUndefined();
        HorizontalLayout buttons = new HorizontalLayout(save);
        save.setStyleName(ValoTheme.BUTTON_PRIMARY);
        save.setClickShortcut(ShortcutAction.KeyCode.ENTER);
        save.addClickListener(e -> save());

        addComponents(name, buttons);

        // Binding of all fields
        bindFileds();
        // Adding tooltips
        setTooltips();
    }

    /**
     * Save changes
     */
    private void save() {
        if (EntityService.getHotelCategoryService().save(category)) {
            setVisible(false);
        }
        hotelCategoryView.updateList();
    }

    /**
     * Edit category
     */
    public void setCategory(HotelCategory category) {
        if (isVisible()) {
            setVisible(false);
        } else {
            this.category = category;
            binder.setBean(category);
            setVisible(true);
            name.selectAll();
        }
    }

    /**
     * Delete selected categories
     * @param categoryList
     */
    public void deleteSelected(List<HotelCategory> categoryList) {
        EntityService.getHotelCategoryService().deleteSelected(categoryList);
        hotelCategoryView.updateList();
    }

    private void bindFileds(){
        binder.forField(name)
                .withValidator(new StringLengthValidator("Name must be between 2 and 20 characters long",2, 20))
                .asRequired("Write category name")
                .bind(HotelCategory::getName, HotelCategory::setName);
    }

    private void setTooltips() {
        name.setDescription("Category name");
    }
}
