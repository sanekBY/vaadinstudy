package com.demo.app.forms;

import com.demo.app.entities.GuarantyFee;
import com.vaadin.data.Binder;
import com.vaadin.data.converter.StringToIntegerConverter;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomField;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.RadioButtonGroup;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

public class GuarantyFeeField extends CustomField<GuarantyFee> {

    Layout vLayout;
    private RadioButtonGroup<Integer> paymentMethods = new RadioButtonGroup<>();

    private TextField creditTextField = new TextField();
    private Label cashLabel = new Label("Payment will be made directly in the hotel");

    private Binder<GuarantyFee> binder = new Binder<>(GuarantyFee.class);

    private GuarantyFee value;
    public GuarantyFeeField() {
        super();
    }

    @Override
    protected Component initContent() {
        vLayout = new VerticalLayout();
        paymentMethods.addStyleName(ValoTheme.OPTIONGROUP_HORIZONTAL);
        paymentMethods.setItems(0, 1);
        paymentMethods.setItemCaptionGenerator(item -> {
            if (item == 0) {
               return  "Credit Card";
            } else return "Cash";
        });

        paymentMethods.addValueChangeListener(event -> {
            if (event.getValue() == 0) {
                generateCashLayout(false);
            }  else {
                generateCashLayout(true);
            }
        });

        creditTextField.addValueChangeListener(event -> {
            if (binder.isValid()) {
                if (value!= null && event.getValue() != null) {
                    value.setGuarantyFee(Integer.parseInt(event.getValue()));
                    fireEvent(new ValueChangeEvent(this, new GuarantyFee(value), false));
                }
            }
        });

        vLayout.addComponent(paymentMethods);
        vLayout.addComponent(cashLabel);

        updateValues();
        bindFileds();

        value = new GuarantyFee();
        return vLayout;
    }

    @Override
    public void doSetValue(GuarantyFee value) {
        this.value = new GuarantyFee(value);
        updateValues();
    }

    @Override
    public GuarantyFee getValue() {
        return value;
    }

    private void updateValues() {
        if (value != null && value.getGuarantyFee() > 0) {
            generateCashLayout(false);
            paymentMethods.setValue(0);
            creditTextField.setValue(getValue().getGuarantyFee().toString());
        } else {
            generateCashLayout(true);
            paymentMethods.setValue(1);
            creditTextField.setValue("0");
        }
    }

    private void generateCashLayout(boolean isCash) {
        if (isCash) {
            vLayout.removeComponent(creditTextField);
            vLayout.addComponent(cashLabel);
            creditTextField.setValue("0");
        } else {
            vLayout.removeComponent(cashLabel);
            vLayout.addComponent(creditTextField);
            creditTextField.setValue("1");
        }
    }

    public void bindFileds(){
        binder.forField(creditTextField)
                 .withConverter(new StringToIntegerConverter(0, "Only digits"))
                .withNullRepresentation(0)
                .withValidator(num -> num <= 100, "Can't be more then 100")
                .withValidator(num -> num >= 0, "Can't be less then 0")
                .bind(GuarantyFee::getGuarantyFee, GuarantyFee::setGuarantyFee);
    }

}
