package com.demo.app.forms;

import com.demo.app.entities.Hotel;
import com.demo.app.services.EntityService;
import com.demo.app.services.HotelCategoryService;
import com.demo.app.services.HotelService;
import com.demo.app.views.HotelListView;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Layout;
import com.vaadin.ui.VerticalLayout;

import java.util.List;

@SuppressWarnings("serial")
@SpringUI
public class HotelBulkUpdateForm extends HotelForm {

    private HotelListView hotelListView;
    private ComboBox<String> select;

    public HotelBulkUpdateForm(HotelListView hotelListView) {
        this.hotelListView = hotelListView;
        init();
    }

    public void init() {

        setSizeUndefined();
        addComponents(createComboBox());

        // Binding of all fields
        bindFileds();
        // Adding tooltips
        setTooltips();

    }

    private Layout createComboBox() {
        Layout vlayout = new VerticalLayout();

        select = new ComboBox<>();
        select.setItems("Name", "Rating", "Address",  "Operates from", "Category", "URL", "Description");


        select.addSelectionListener(event -> {
            vlayout.removeAllComponents();
            switch (select.getSelectedItem().get()) {
                case "Operates from" : {
                    vlayout.addComponents(select, operatesFrom);
                    break;
                }
                case "Name" : {
                    vlayout.addComponents(select, name);
                    break;
                }
                case "Rating" : {
                    vlayout.addComponents(select, rating);
                    break;
                }
                case "Address" : {
                    vlayout.addComponents(select, address);
                    break;
                }
                case "Category" : {
                    category.setItems(getAllCategories());
                    vlayout.addComponents(select, category);
                    break;
                }
                case "URL" : {
                    vlayout.addComponents(select, url);
                    break;
                }
                case "Description" : {
                    vlayout.addComponents(select, description);
                    break;
                }
            }
        });

        vlayout.addComponent(select);

        return vlayout;
    }

    /**
     * Save changes
     */
    public void update(List<Hotel> hotels) {
        EntityService.getHotelService().editHotels(modifyHotelData(hotels));
    }

    public List<Hotel> modifyHotelData(List<Hotel> hotelList) {
        for (Hotel hotel : hotelList) {
            if (select.getSelectedItem().get().equals("Name")) {
                hotel.setName(name.getValue());
            } else if (select.getSelectedItem().get().equals("Rating")) {
                hotel.setRating(Integer.parseInt(rating.getValue()));
            } else if (select.getSelectedItem().get().equals("Address")) {
                hotel.setAddress(address.getValue());
            } else if (select.getSelectedItem().get().equals("Operates from")) {
                hotel.setOperatesFrom(operatesFrom.getValue().toEpochDay());
            } else if (select.getSelectedItem().get().equals("Category")) {
                hotel.setCategory(category.getValue());
            } else if (select.getSelectedItem().get().equals("URL")) {
                hotel.setUrl(url.getValue());
            } else if (select.getSelectedItem().get().equals("Description")) {
                hotel.setDescription(description.getValue());
            }
        }

        return hotelList;
    }


}
