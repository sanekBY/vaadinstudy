package com.demo.app.forms;

import com.demo.app.converters.DateConverter;
import com.demo.app.entities.GuarantyFee;
import com.demo.app.entities.Hotel;
import com.demo.app.entities.HotelCategory;
import com.demo.app.services.EntityService;
import com.demo.app.views.HotelListView;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.data.converter.StringToIntegerConverter;
import com.vaadin.data.validator.LongRangeValidator;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.Button;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.themes.ValoTheme;
import java.util.List;

import static com.vaadin.event.ShortcutAction.KeyCode;

@SuppressWarnings("serial")
@SpringUI
public class HotelForm extends FormLayout {

    public TextField name = new TextField("Name");
    public TextField address = new TextField("Address");
    public TextField rating = new TextField("Rating");
    public NativeSelect<HotelCategory> category = new NativeSelect<>("Category");
    public DateField operatesFrom = new DateField("Operates from");
    public TextField description = new TextField("Description");
    public TextField url = new TextField("Url");
    private Button save = new Button("Save");
    public GuarantyFeeField guarantyFeeField = new GuarantyFeeField();
    private Notification notif = new Notification("");
    private Integer guarantyOld = new Integer(-1);

    private Hotel hotel;
    private HotelListView hotelListView;
    private Binder<Hotel> binder = new Binder<>(Hotel.class);

    public HotelForm(HotelListView hotelListView) {
        this.hotelListView = hotelListView;
        init();

    }

    public HotelForm() {
    }

    public HotelForm(TextField name) {
        this.name = name;
    }

    public void init() {
        setMargin(false);
        setSizeUndefined();
        HorizontalLayout buttons = new HorizontalLayout(save);
        addComponents(name, address, rating, category, guarantyFeeField, operatesFrom, url, description, buttons);

        // Add hotel categories
        category.setItems(getAllCategories());
        category.setItemCaptionGenerator(HotelCategory::getName);
        category.setEmptySelectionAllowed(false);

        save.setStyleName(ValoTheme.BUTTON_PRIMARY);
        save.setClickShortcut(KeyCode.ENTER);

        guarantyFeeField.addValueChangeListener( event -> {
            generateNotification(guarantyFeeField.getValue());
        });

        // Binding of all fields
        bindFileds();
        // Adding tooltips
        setTooltips();

        save.addClickListener(e -> save());

    }

	/**
	 * Generate notification
	 * @param guarantyFee
	 */
	private void generateNotification(GuarantyFee guarantyFee){
        if (guarantyFee != null) {
            notif.setCaption("Value has chenged from " +  generateText(guarantyOld) + "to " + generateText(guarantyFee.getGuarantyFee()));
            notif.show(UI.getCurrent().getPage());
            guarantyOld = guarantyFee.getGuarantyFee();
        }
    }

	/**
	 * Generate text for notification
	 * @param num
	 * @return
	 */
	private String generateText(Integer num) {
        if (num == 0) {
            return "cash ";
        } else {
            return "credit card " + num + "% ";
        }
    }

    /**
     * Add hotel to form
     */
    public void setHotel(Hotel hotel) {
        if (isVisible()) {
            setVisible(false);
        } else {
            this.hotel = hotel;
            binder.setBean(hotel);
            setVisible(true);
            name.selectAll();
        }
    }

    public void setNewHotel() {
        if (isVisible()) {
            setVisible(false);
        } else {
            this.hotel = new Hotel();
            binder.setBean(hotel);
            setVisible(true);
            name.selectAll();
        }
    }

    /**
     * Delete hotel
     */
    private void delete() {
        EntityService.getHotelService().delete(hotel);
        hotelListView.updateList();
        setVisible(false);
    }

    /**
     * Save changes
     */
    private void save() {
	    binder.validate();
	    if (binder.isValid()) {
		    try {
			    binder.writeBean(hotel);

			    if (EntityService.getHotelService().save(hotel)) {
				    setVisible(false);
				    hotelListView.updateList();
			    }
		    } catch (ValidationException e) {
			    e.printStackTrace();
		    }
	    } else {
	    	notif.setCaption("There are some unvalid values, please check the input and try again.");
		    notif.show(UI.getCurrent().getPage());
	    }
    }

    /**
     * Delete selected hotels
     */
    public void deleteSelected(List<Hotel> hotels) {
        EntityService.getHotelService().deleteSelected(hotels);
        hotelListView.updateList();
    }

    /**
     * Get hotel categories
     */
    public List<HotelCategory> getAllCategories() {
        return EntityService.getHotelCategoryService().findAll();
    }

    public void bindFileds(){
        binder.forField(name)
                .withValidator(new StringLengthValidator("Name must be between 2 and 20 characters long",2, 20))
                .asRequired("Please enter a name")
                .bind(Hotel::getName, Hotel::setName);
        binder.forField(address)
                .withValidator(new StringLengthValidator("Enter address",1, 100))
                .asRequired("Please enter an address")
                .bind(Hotel::getAddress, Hotel::setAddress);
        binder.forField(rating)
                .withConverter(new StringToIntegerConverter(0, "Only digits"))
                .withNullRepresentation(0)
                .withValidator(num -> num < 6, "Can't be more then 5")
                .withValidator(num -> num > 0, "Can't be less then 1")
                .asRequired("Please enter a rating")
                .bind(Hotel::getRating, Hotel::setRating);
        binder.forField(operatesFrom)
                .withConverter(new DateConverter())
                .withValidator(new LongRangeValidator("Wrong date", (long) 0, (long) 365*30)) // hotel can't be opened more then 30 years ago
                .asRequired("Please enter enter a date")
                .bind(Hotel::getOperatesFrom, Hotel::setOperatesFrom);
        binder.forField(category)
                .asRequired("Please enter a category")
                .bind(Hotel::getCategory, Hotel::setCategory);
        binder.forField(url)
                .withValidator(new StringLengthValidator("Wrong url",4, 8000))
                .asRequired("Please enter url")
                .bind(Hotel::getUrl, Hotel::setUrl);
        binder.forField(description)
                .bind(Hotel::getDescription, Hotel::setDescription);
        binder.forField(guarantyFeeField).bind(Hotel::getGuarantyFee, Hotel::setGuarantyFee);
    }

    public void setTooltips() {
        name.setDescription("Hotel name");
        address.setDescription("Hotel address");
        rating.setDescription("Rating of a hotel");
        category.setDescription("Category of hotel");
        operatesFrom.setDescription("Operates from");
        description.setDescription("Description of a hotel");
        url.setDescription("Link");
        save.setDescription("Save changes");
    }

}
