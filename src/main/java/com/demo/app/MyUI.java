package com.demo.app;

import com.demo.app.services.EntityService;
import com.demo.app.views.HotelCategoryView;
import com.demo.app.views.HotelListView;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.spring.annotation.EnableVaadin;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.ContextLoaderListener;

import javax.servlet.annotation.WebListener;
import javax.servlet.annotation.WebServlet;

@SuppressWarnings("serial")
@Theme("mytheme")
@SpringUI
//@Widgetset("com.demo.app.MyAppWidgetset")
public class MyUI extends UI {

    private Navigator navigator;

    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
    public static class HotelUIServlet extends VaadinServlet {}

    @Configuration
    @EnableVaadin
    public static class MyConfiguration {
        @Bean
        public static EntityService getEntityService() {
            return new EntityService();
        };
    }

    @WebListener
    public static class MyContextLoaderListener extends ContextLoaderListener {}

    @Override
    protected void init(VaadinRequest vaadinRequest) {

        VerticalLayout mainLayout = new VerticalLayout();
        setContent(mainLayout);

        CssLayout content = new CssLayout();
        content.setSizeFull();

        mainLayout.addComponent(createMenuBar());
        mainLayout.addComponent(content);

        createNavigationBar(content);
    }

    private MenuBar createMenuBar() {
        MenuBar mainMenuBar = new MenuBar();

        mainMenuBar.addItem("Hotels list", new MenuBar.Command() {
            public void menuSelected(MenuBar.MenuItem selectedItem) {
                navigator.navigateTo("hotel-list");
            }
        });

        mainMenuBar.addItem("Hotel categories", new MenuBar.Command() {
            public void menuSelected(MenuBar.MenuItem selectedItem) {
                navigator.navigateTo("hotel-category");
            }
        });

        return mainMenuBar;
    }

    private Navigator createNavigationBar(CssLayout content) {
        navigator = new Navigator(this, content);
        navigator.addView("hotel-list", HotelListView.class);
        navigator.addView("hotel-category", HotelCategoryView.class);
        navigator.navigateTo("hotel-list");
        return navigator;
    }

}
