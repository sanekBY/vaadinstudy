package com.demo.app.converters;

import com.vaadin.data.Converter;
import com.vaadin.data.Result;
import com.vaadin.data.ValueContext;

import java.time.Duration;
import java.time.LocalDate;

public class DateConverter implements Converter<LocalDate, Long>{

    @Override
    public Result<Long> convertToModel(LocalDate localDate, ValueContext valueContext) {
        if (localDate != null) {
            long daysCount= Duration.between(localDate.atStartOfDay(), LocalDate.now().atStartOfDay()).toDays();
            return Result.ok(daysCount);
        }
        return Result.ok(null);
    }

    @Override
    public LocalDate convertToPresentation(Long aLong, ValueContext valueContext) {
        if (aLong != null) {
            return LocalDate.now().minusDays(aLong);
        }
        return LocalDate.now();
    }

    @Override
    public <T> Converter<LocalDate, T> chain(Converter<Long, T> other) {
        return null;
    }
}
