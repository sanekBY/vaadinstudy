package com.demo.app.services;


import com.demo.app.entities.GuarantyFee;
import com.demo.app.entities.Hotel;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class HotelService {

    @PersistenceContext(unitName = "demo_hotels", type = PersistenceContextType.TRANSACTION)
    private EntityManager entityManager;

    public HotelService() {
    }

    @Transactional
    public synchronized List<Hotel> findAll(String stringFilterName, String stringFilterAddress) {
        List<Hotel> hotels = new ArrayList<>();

        hotels = entityManager.createNamedQuery("Hotel.filterByNameAddress", Hotel.class).
                setParameter("name", "%" + stringFilterName.toLowerCase() + "%").
                setParameter("address", "%" + stringFilterAddress.toLowerCase() + "%").
                getResultList();

        return hotels;
    }

    @Transactional
    public boolean save(Hotel hotel) {
        if (hotel == null || hotel.getName() == null || hotel.getRating() == null ||
                hotel.getAddress() == null || hotel.getUrl() == null) {
            return false;
        }
        if (hotel.getId() == null) {
            hotel.setGuarantyFee(new GuarantyFee());
            entityManager.persist(hotel);
        } else {
            entityManager.merge(hotel);
        }

        return true;
    }

    /**
     * Edit selected hotels
     * @param hotels
     */
    @Transactional
    public synchronized void editHotels(List<Hotel> hotels) {

        for (Hotel hotel : hotels) {
            entityManager.merge(hotel);
        }

    }

    public void delete(Hotel value) {

        value = entityManager.find(Hotel.class, value.getId());
        entityManager.remove(value);

    }

    /**
     * Delete selected hotels
     */
    @Transactional
    public synchronized void deleteSelected(List<Hotel> hotelList) {

        for (Hotel hotel : hotelList) {
            delete(hotel);
        }

    }


}
