package com.demo.app.services;

import com.demo.app.entities.HotelCategory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.util.List;

@Repository
public class HotelCategoryService {

	@PersistenceContext(unitName = "demo_hotels", type = PersistenceContextType.TRANSACTION)
	private EntityManager entityManager;

	public HotelCategoryService() {
	}

	@Transactional
	public synchronized List<HotelCategory> findAll() {
	    List<HotelCategory> hotelCategories = entityManager.createNamedQuery("HotelCategory.findAll", HotelCategory.class).getResultList();
        return hotelCategories;
	}

	/**
	 * Delete one category
	 */
	public synchronized void delete(HotelCategory category) {
		category = entityManager.find(HotelCategory.class, category.getId());
		entityManager.remove(category);
	}

	/**
	 * Delete selected categories
	 * @param categoryList
	 */
	@Transactional
	public synchronized void deleteSelected(List<HotelCategory> categoryList) {
        for (HotelCategory category : categoryList) {
			delete(category);
		}
	}

	/**
	 * Save changes
	 * @param category
	 * @return
	 */
	@Transactional
	public synchronized boolean save(HotelCategory category) {
		if (category.getName() == null) {
			return false;
		}

		try {
		    if (category.getId() == null) {
                entityManager.persist(category);
            } else {
                entityManager.merge(category);
            }
		} catch (Exception ex) {
			ex.printStackTrace();
		}
        return true;
	}

}
